#!/bin/bash

# test rclone to cloud storage running as anacron job.

if [ -d /home/demo/Live-usb-storage/opt/influxdbV2 ];
then 
	# search for files recursively up to 3 depths in the file tree 
	# handles folder structure up to: parent/child/grandchild/
	if [ "$(find /home/demo/Live-usb-storage/opt/influxdbV2/ -maxdepth 3 -type f)" ];
	then
		rclone sync --progress /home/demo/Live-usb-storage/opt/influxdbV2 "idrive2:influxdbV2" --filter-from .rcignore;
	else
		printf "influxdb folder is empty and no action is taken \n";
	fi
else
        printf "influxdb folder does not exist \n";	
fi
