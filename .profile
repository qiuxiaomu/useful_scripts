# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login # exists.  # see /usr/share/doc/bash/examples/startup-files for examples.  # the files are located in the bash-doc package.  # the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash 
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

export QT_QPA_PLATFORMTHEME="qt5ct"
export EDITOR=/usr/bin/vim
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
# fix "xdg-open fork-bomb" export your preferred browser from here
export BROWSER=/usr/bin/palemoon
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

if [ -f ~/.git-prompt.sh ]; then
  source ~/.git-prompt.sh
fi

# Unclutter: auto hide mouse if idle
unclutter -idle 10 &

export PS1='\[\033[0;32m\]\[\033[0m\033[0;32m\]\u\[\033[0;36m\] @ \[\033[0;36m\]\h \w\[\033[0;32m\]$(__git_ps1)\n\[\033[0;32m\]└─\[\033[0m\033[0;32m\] \$\[\033[0m\033[0;32m\] ▶\[\033[0m\]'

export PATH="/home/demo/Live-usb-storage/opt/bin:/home/demo/Live-usb-storage/scripts/:/home/demo/Live-usb-storage/opt/julia-1.8.3/bin/:$PATH"

# pipx env variables.
PIPX_HOME="$HOME/Live-usb-storage/opt/pipx"
PIPX_BIN_DIR="$HOME/Live-usb-storage/opt/pipx/bin"


# For pyenv startup.
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# gitlab token
export gkey="glpat-nx1g575GwsxU4oK_vNPQ"

# github token
export hkey="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"

# influxdb configs 
export operator_token="noV-EtX3SZIIyFDcMKCm6TR-CC0BHtMqwqRkpjC2eTC9Usef7DDgRPM_NGrZSGw6fM2FSMdBo1YfG2KboKJLZw=="
export master_token="XdkrjxyAcsL51PcgtXOe9xg8wdGH-NrHU0R_kDhS8YmOdbTPaiWBYrtg-AYyEDkIdq_YVuhf40x-pNvtvwR-KA=="
export q_org_token="noV-EtX3SZIIyFDcMKCm6TR-CC0BHtMqwqRkpjC2eTC9Usef7DDgRPM_NGrZSGw6fM2FSMdBo1YfG2KboKJLZw=="
export INFLUX_TOKEN="noV-EtX3SZIIyFDcMKCm6TR-CC0BHtMqwqRkpjC2eTC9Usef7DDgRPM_NGrZSGw6fM2FSMdBo1YfG2KboKJLZw=="
export INFLUXD_ENGINE_PATH="/home/demo/Live-usb-storage/opt/influxdbV2/engine/"
export INFLUXD_BOLT_PATH="/home/demo/Live-usb-storage/opt/influxdbV2/influxd.bolt"
export INFLUXD_SQLITE_PATH="/home/demo/Live-usb-storage/opt/influxdbV2/influxd.sqlite"
# influx cli config
export INFLUXD_CONFIGS_PATH="/home/demo/Live-usb-storage/opt/influxdbV2/configs"
# influxd server config
export INFLUXD_CONFIG_PATH="/home/demo/Live-usb-storage/opt/influxdbV2/config.yaml"

# dolphindb config
export LD_LIBRARY_PATH="/home/demo/Live-usb-storage/opt/dolphindb/:$LD_LIBRARY_PATH"

# set vi mode at prompt
set -o vi

. "$HOME/.cargo/env"
export RUSTUP_HOME="/home/demo/Live-usb-storage/opt/.rustup"
export CARGO_HOME="/home/demo/Live-usb-storage/opt/.cargo"
. "/home/demo/Live-usb-storage/opt/.cargo/env"
