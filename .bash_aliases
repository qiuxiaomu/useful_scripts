# Utility Commands
alias lx='ls -larth'
alias xc='x && x normal && x primary 4 && x rotate 4 normal && x repos'
alias vn='virtualenv -p python3'
alias act='source bin/activate'
alias dact='deactivate'
alias see='feh'
alias dupe='xclip -selection c'
alias tanacron='anacron -T -t ~/.anacron/etc/anacrontab -S ~/.anacron/spool/'

# GIT Commands
alias gt='git status'
alias go='git checkout'
alias new='git checkout -b'
alias delf='git branch -D'
alias track='git push -u origin'
alias graph='git log --oneline --abbrev-commit --all --graph --decorate --color'
alias rem='git branch -r'
alias tagsr='git ls-remote --tags'
alias loc='git branch -l' 
alias update='git pull origin'

# AUR Commands
alias sr='yay -Ss'
alias get='yay -S'
alias out='yay -R'
alias pbd='yay -G'
alias up='yay -Syu --devel --timeupdate'
alias clean='yay -c'

# git add files and commit.
function ac(){

   git add $1 && git commit -m "$2"

}

# git add all files and commit with message.
function acc(){

   git add . && git commit -m "$1"

}


# function to temporarily change prompt handle.
# probably should move this to its own file.
function prom(){

	if [ ! -z "$1" -a "$1" != "" ]
	then
	    if [[ $1 = *$ ]]
	    then
	        PS1="$1 "
            else
	        echo "prompt handle ends with dollar sign."; set -e;
	    fi	
	else
	    echo "please enter a valid prompt handle."; set -e;
    	fi	    
}

